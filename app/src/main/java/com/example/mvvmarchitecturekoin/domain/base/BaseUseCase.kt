package com.example.mvvmarchitecturekoin.domain.base

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class BaseUseCase<in Params, T> {

    private val disposables = CompositeDisposable()

    abstract fun buildUseCaseObservable(): Single<T>

    open fun execute(observer: DisposableSingleObserver<T>) {
        val observable = this.buildUseCaseObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()) as Single<T>
        disposables.add(observable.subscribeWith(observer))
    }

    fun dispose() {
        if (!disposables.isDisposed) disposables.dispose()
    }

}
