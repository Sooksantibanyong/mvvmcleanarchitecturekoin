package com.example.mvvmarchitecturekoin.ui.first

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvmarchitecturekoin.R
import com.example.mvvmarchitecturekoin.extensions.observe
import com.example.mvvmarchitecturekoin.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class FirstActivity : AppCompatActivity() {

    private val viewModel: NewsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeNewsList()

        viewModel.getNewsList()
    }

    private fun observeNewsList() {
        observe(viewModel.newsList){
            //To do something
            Log.i("AAAAA1","Yessss")
        }
        observe(viewModel.errorNewsList){
            //To do something
        }
    }
}