package com.example.mvvmarchitecturekoin.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmarchitecturekoin.data.response.Article
import com.example.mvvmarchitecturekoin.domain.usecase.GetNewsListUseCase
import com.example.mvvmarchitecturekoin.viewmodel.base.BaseDisposeSingle

class NewsViewModel(private val getNewsListUseCase: GetNewsListUseCase) : ViewModel() {

    val newsList = MutableLiveData<List<Article>>()
    val errorNewsList = MutableLiveData<String>()

    fun getNewsList(){
        getNewsListUseCase.execute(object : BaseDisposeSingle<List<Article>>(){
            override fun success(t: List<Article>) {
                newsList.value = t
            }

            override fun error(e: Throwable) {
                errorNewsList.value = null
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        getNewsListUseCase.dispose()
    }

}
