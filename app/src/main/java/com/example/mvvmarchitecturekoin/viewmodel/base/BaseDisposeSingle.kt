package com.example.mvvmarchitecturekoin.viewmodel.base

import io.reactivex.observers.DisposableSingleObserver

abstract class BaseDisposeSingle<T> : DisposableSingleObserver<T>() {
    override fun onSuccess(t: T) {
        success(t)
    }

    override fun onError(e: Throwable) {
        error(e)
    }

    abstract fun success(t: T)
    abstract fun error(e : Throwable)
}