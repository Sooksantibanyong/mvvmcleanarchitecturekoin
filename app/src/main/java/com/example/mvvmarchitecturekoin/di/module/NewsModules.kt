package com.example.mvvmarchitecturekoin.di.module

import com.example.mvvmarchitecturekoin.data.repository.NewsRepository
import com.example.mvvmarchitecturekoin.data.repository.NewsRepositoryImpl
import com.example.mvvmarchitecturekoin.domain.usecase.GetNewsListUseCase
import com.example.mvvmarchitecturekoin.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {

    factory<NewsRepository> { NewsRepositoryImpl(get()) }

    viewModel { NewsViewModel(get()) }

    single { GetNewsListUseCase(get()) }

}
