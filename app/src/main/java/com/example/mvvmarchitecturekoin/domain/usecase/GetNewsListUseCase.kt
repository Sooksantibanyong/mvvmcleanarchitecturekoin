package com.example.mvvmarchitecturekoin.domain.usecase

import com.example.mvvmarchitecturekoin.data.repository.NewsRepository
import com.example.mvvmarchitecturekoin.data.response.Article
import com.example.mvvmarchitecturekoin.domain.base.BaseUseCase
import io.reactivex.Single

class GetNewsListUseCase constructor(
    private val repository: NewsRepository
): BaseUseCase<Any, List<Article>>() {

    override fun buildUseCaseObservable(): Single<List<Article>> {
        return repository.getNewsApi()
            .onErrorResumeNext {
                Single.error(it)
            }
    }
}
