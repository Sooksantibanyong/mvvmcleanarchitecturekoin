package com.example.mvvmarchitecturekoin.data.api

import com.example.mvvmarchitecturekoin.data.response.NewsDataModel
import io.reactivex.Single
import retrofit2.http.GET

interface NewsApi {

    @GET("top-headlines?sources=abc-news&apiKey=ce6cca4d41c8472ba2e4acd1d7064fd6")
    fun getNews(): Single<NewsDataModel>

}
