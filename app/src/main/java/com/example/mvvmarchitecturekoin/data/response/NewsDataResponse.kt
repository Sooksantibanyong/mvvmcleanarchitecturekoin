package com.example.mvvmarchitecturekoin.data.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsDataModel(
    @SerializedName("status")
    var status: String? = null,
    @SerializedName("articles")
    var articles: List<Article>? = null
): Parcelable

@Parcelize
data class Article (
    @SerializedName("author")
    var author: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("urlToImage")
    var urlToImage: String? = null,
    @SerializedName("publishedAt")
    var publishedAt: String? = null
): Parcelable
