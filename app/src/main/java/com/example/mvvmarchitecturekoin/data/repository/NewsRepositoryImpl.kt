package com.example.mvvmarchitecturekoin.data.repository

import com.example.mvvmarchitecturekoin.data.api.NewsApi
import com.example.mvvmarchitecturekoin.data.response.Article
import io.reactivex.Single

interface NewsRepository{
    fun getNewsApi(): Single<List<Article>>
}

class NewsRepositoryImpl constructor(private val newsApi: NewsApi): NewsRepository {

    override fun getNewsApi(): Single<List<Article>> {
        return newsApi.getNews()
            .map {
                it.articles
            }
    }
}
